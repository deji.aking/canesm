Contributing to CanESM (Developers guide)
=========================================

For a quickstart on modifying and contributing back to ``CanESM``, readers
are directed to the :ref:`modifying CanESM quickstart guide <Modifying CanESM>`.
The sections below provide a more comprehensive conceptual overview of the
workflows used to develop CanESM, detailed technical guidance on interacting
with the CanESM version control system, and applicable code standards.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   canesm-vcs
   fortran-standard
   dev-cycle






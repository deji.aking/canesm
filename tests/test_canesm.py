import sys
import time
import pandas as pd
import os
import pytest
import yaml
from util import generate_config_file


sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble


@pytest.fixture
def canesm_ensemble():

    created_ensembles = []

    def _canesm_ensemble(ensemble_name):
        # setup code
        config_file = os.path.join('tests', 'config_files', f'{ensemble_name}.yaml')
        sha_file = os.path.join('tests', 'config_files', f'{ensemble_name}.sha')

        newfile = generate_config_file(config_file)
        ens = CanESMensemble.from_config_file(newfile)
        ens.submit_ensemble = True
        ens.setup_ensemble()

        sha = pd.read_csv(sha_file, delim_whitespace=True, header=None)

        ens_info = {'ensemble': ens, 'sha': sha}
        created_ensembles.append(ens_info)
        return ens_info

    yield _canesm_ensemble

    # teardown code
    for ensemble in created_ensembles:
        ens = ensemble['ensemble']
        # delete files from ppp
        for job in ens.jobs:
            runpath = job.runpath.replace('work', 'sitestore')
            datapath = os.path.dirname(runpath)  # drop the /data folder
            job.run_command(f'chmod -R u+w {datapath}', setup_env=False)
            job.run_command(f'rm -rf {datapath}', setup_env=False)

        # delete from backend
        ens.delete_ensemble()


def check_ensemble(ensemble_info):

    ens = ensemble_info['ensemble']
    sha = ensemble_info['sha']

    # pause until run is finished
    # TODO: extend this to handle > 1 member
    job = ens.jobs[0]
    events = job.events
    run_finished = False
    while not run_finished:
        for e in events.Event.values:
            if 'create-dump-job-string' in e:
                run_finished = True
                break
        else:
            print('waiting for output files...')
            time.sleep(60)
            events = job.events

    print('diagnostics running...')
    time.sleep(15 * 60)  # pause while diagnostics run
    runpath = job.runpath.replace('work', 'sitestore')

    for s, file in sha.values:
        file = file.replace('{runid}', job.runid)
        remote_sha = job.run_command(f'sha1sum {file}', setup_env=False, run_directory=runpath)
        remote_sha = remote_sha.stdout.strip().split()[0]
        if s != remote_sha:
            raise ValueError(f'sha from {file} does not match expected')

    return 1


def test_ensemble(canesm_ensemble):

    tests = yaml.load(open(os.path.join('tests', 'ensemble_tests.yml')), Loader=yaml.SafeLoader)
    for ensemble_name in tests['test_ensembles']:
        ens_info = canesm_ensemble(ensemble_name)
        assert check_ensemble(ens_info) == 1

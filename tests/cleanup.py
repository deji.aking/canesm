from .util import delete_ensembles


if __name__ == '__main__':
    try:
        delete_ensembles()
    except FileNotFoundError as exc:
        print(f"Existing ensemble files not found - skipping cleanup")

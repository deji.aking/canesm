import sys
import os
from .util import get_test_ensembles

# add ensemble tool, within CCCma tools, to the python path
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'CCCma_tools', 'ensemble'))
from canesm import CanESMensemble, CanESMsetup

def test_build():
    """
        Setup continuous test run, failing if executables don't get compiled
    """

    # get all the ci ensembles, and loop through, submitting each one
    for ensemble in get_test_ensembles():
        ens = ensemble.ensemble

        # setup and submit
        ens.submit_ensemble = True
        ens.setup_ensemble()

        # get information on the ONE job (hard coded), a loop should be added if more ensemble members are added
        job = ens.jobs[0]

        # initiate information about potential executables
        atmos   = { 'exists'   : False, 
                    'location' : f'{os.path.join(job.ccrnsrc,"executables")}',
                    'file_pattern' : 'canam.exe' }
        coupler = { 'exists'   : False, 
                    'location' : f'{os.path.join(job.ccrnsrc,"executables")}', 
                    'file_pattern' : 'cancpl.exe'}
        ocean   = { 'exists'   : False, 
                    'location' : f'{os.path.join(job.ccrnsrc,"executables")}', 
                    'file_pattern' : 'nemo.exe' }

        if job.config == 'ESM':
            exes = {'atmos': atmos, 'coupler' : coupler, 'ocean' : ocean }
        elif job.config == 'AMIP':
            exes = {'atmos': atmos, 'coupler' : coupler}
        elif job.config == 'OMIP':
            exes = {'ocean' : ocean }
        else:
            raise ValueError('unrecognized runmode')

        # check that the execs have been produced
        check_for_exes(exes, job)

        # raise error if any failed
        for exe, exe_info in exes.items():
            assert exe_info['exists'], f'{exe} executable not found! Did compilation fail?????'

def check_for_exes(exes, job):
    """
        Given a dictionary describing the desired executables, confirm that they have been
        compiled successfully
    """
    for exe, exe_info in exes.items():
        # build suitable find command
        #   - '-not xtype l' avoids broken links being returned
        exe_pat  = exe_info['file_pattern']
        file_loc = exe_info['location']
        find_cmd = f'find {file_loc}/ -maxdepth 1 -not -xtype l -name "{exe_pat}"'

        # check for existence of exec
        find_output = job.run_command(find_cmd)

        # find should have returned output if exe exists
        if find_output.stdout.strip():
            exe_info['exists'] = True
    

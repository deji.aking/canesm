#!/bin/bash
set -e
#
# load files from tape and save them for a new runid / year
#===========================================================

# define usage message
Usage='Usage:

  tapeload_rs [-a nl_file1] [-c nl_file2] [-n nl_file3] MODEL_JOB
              [config_file=/path/to/file.cfg]

    Retrieve restart archives from tape and save processed version for this run.

    Args: 
        MODEL_JOB   : defines the jobstring used to extract critical model parameters
        config_file : [optional] path to file containing the high level config settings 
                        for this run (i.e. parent runid). Defaults to `canesm.cfg` in the 
                        working directory.

    Flags:
        -a : tells the script to pull the given agcm namelist file from the extracted agcm 
             restart and use it for the run. Must be used multiple times if multple namelists are
             desired.
        -c : tells the script to pull the given coupler namelist file from the extracted coupler
             restart and use it for the run. Must be used multiple times if multiple namelists are
             desired.
        -n : tells the script to pull the given nemo namelist file from the extracted nemo
             restart and use it for the run. Must be used multiple times if multiple namelists are
             desired.
'
config_file="canesm.cfg" # unless user specifies this file assume it is present in calling directory
call_dir=$(pwd)
#~~~~~~~~~~~~~~~
# Parse Options
#~~~~~~~~~~~~~~~
# build arrays of namelist files to pull from the restarts and use for the run
while getopts a:n:c:h opt; do
    case $opt in
        a) agcm_restart_namelists_to_use+=("$OPTARG");;
        n) nemo_restart_namelists_to_use+=("$OPTARG");;
        c) coupler_restart_namelists_to_use+=("$OPTARG");;
        h) echo "$Usage"; exit 0;;
        *) echo "$Usage"; exit 1;;
    esac
done
shift $(( OPTIND - 1 ))

#~~~~~~~~~~~~~~~~~
# Parse Arguments
#~~~~~~~~~~~~~~~~~
for arg in "$@"; do
    case $arg in
        *=*)
            var=$(echo $arg | awk -F\= '{printf "%s",$1}')
            val=$(echo $arg | awk -F\= '{printf "%s",$2}')
            case $var in
                config_file) config_file="$val" ;; # file containing high level config settings
                          *) bail "Invalid command line arg --> $arg <--" ;;
            esac ;;
          *) MODEL_JOB=$arg ;;
    esac
done
[[ -z $config_file ]] && { echo "save_restart_files needs to know the config file"; exit 1;}
source ${config_file}

# get useful shell functions
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Perform some sanity checks
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# check that the model job is provided and exists
if ! is_defined $MODEL_JOB || is_zero_size $MODEL_JOB; then
  echo "$Usage"
  exit 1
fi
echo MODEL_JOB=$MODEL_JOB

# check that $runid=$runid_env
if [[ $runid != $runid_env ]]; then
  echo "ERROR: runid=$runid is not the same as runid_env=$runid_env."
  echo "Make sure to source env_setup_file."
  exit 1
fi

# Run code checking and update logs
[ $production -eq 1 ] && flgs="" || flgs="-d"   # use 'development' mode if production is off
strict_check $flgs $runid load-from-tape canesm.cfg tapeload_rs_${runid}
[ $? -ne 0 ] && exit 1

#~~~~~~~~~~~~~~~~~~~~~~~~~
# Set inherited variables
#~~~~~~~~~~~~~~~~~~~~~~~~~
run_namelist_dir=${WRK_DIR}/config/namelists

# parent information is taken from canesm.cfg file but can be overwritten below
runid_in=${override_restart_runid:-$parent_runid}       # Parent runid to be used as restart
date_in=${override_restart_time:-$parent_branch_time}   # Restart date in the parent run as YYYY_m12

# use parent archive settings from canesm.cfg
archive_user=${parent_archive_user}     # username of the RS archive, if you know (e.g cpd103)
archive_class=${parent_archive_class}   # long or short term class

# set hpcarchive user flag
if [[ $archive_user == unknown ]] || ! is_defined $archive_user ; then
    # search over all users
    archive_user_arg="-U"
else
    # search for given user archives
    archive_user_arg="-u $archive_user"
fi

# check what restarts are needed and throw errors if user provided erroneous options for namelists
case $config in
     ESM) save_agcm_files=1; save_coupler_files=1; save_nemo_files=1;
          required_restarts="CanAM: mc_${runid_in}_${date_in} CanCPL: mc_${runid_in}_${date_in} CanNEMO: mc_${runid_in}_${date_in}";;
    AMIP) save_agcm_files=1; save_coupler_files=1; save_nemo_files=0;
          required_restarts="CanAM: mc_${runid_in}_${date_in} CanCPL: mc_${runid_in}_${date_in}";
          (( ${#nemo_restart_namelists_to_use[@]} )) && bail "The use of nemo namelists isn't valid for an AMIP run!" ;;
    OMIP) save_agcm_files=0; save_coupler_files=0; save_nemo_files=1;
          required_restarts="CanNEMO: mc_${runid_in}_${date_in}"; 
          (( ${#agcm_restart_namelists_to_use[@]} ))    && bail "The use of agcm namelists isn't valid for an OMIP run!";
          (( ${#coupler_restart_namelists_to_use[@]} )) && bail "The use of coupler namelists isn't valid for an OMIP run!" ;;
esac

# derive start_year and start_month
start_date=(${start_time//:/ })
start_year=${start_date[0]:-1}
start_month=${start_date[1]:-1}

# derive date_out from start_year/start_month
if (( $start_month == 1 )) ; then
  year_out=$(( start_year - 1 ))
  year_out=$(pad_integer $year_out 4 )
  month_out=12
else
  year_out=$start_year
  month_out=$(( start_month - 1 ))
  month_out=$(pad_integer $month_out 2 )
fi

runid_out=${runid}                 # runid of your run
date_out=${year_out}_m${month_out} # restart date of your run YYYY_m12
echo runid_in=${runid_in} date_in=${date_in}
echo runid_out=${runid_out} date_out=${date_out}

# extract year from date_out
year_out=$(echo $date_out | cut -f1 -d'_')
month_out=$(echo $date_out | cut -f2 -d'_' | cut -c2-3)
# initial year
if (( month_out == 12 )) ; then
  iyear=$(( year_out + 1 ))
else
  iyear=$year_out
fi
echo year_out=$year_out month_out=$month_out iyear=$iyear

# compare restart months
month_in=$(echo $date_in | cut -f2 -d'_' | cut -c2-3)
if (( month_in != month_out )) ; then
  echo "month_in=$month_in month_out=$month_out"
  echo "ERROR in tapeload_rs: Input and output restart months must be the same."
  exit 1
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Retrieve, Process, and Save restart files
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# create a temp dir not to interfere with files in local directory
tmpdir=$RUNPATH/tmp.restart.$$

# set a trap to remove $tmpdir on exit
trap "cleanup" 0 1 2 3 6 15
cleanup()
{
  exit_status=$?
  echo exit_status=$exit_status
  echo "Caught Signal ... cleaning up."
  rm -rf $tmpdir
  echo "Done cleanup ... quitting."
  if (( exit_status != 0 )) ; then
    echo "*** ERROR in the tapeload_rs script!!! ***"
    echo "Please verify input restart files are archived:"
    echo "$required_restarts"
  fi
  exit $exit_status
}
mkdir -p $tmpdir
cd $tmpdir

# copy model job string into current directory
cp -p $call_dir/$MODEL_JOB MODEL_JOB

# pull archive that contains the restarts
#-----------------------------------------
# first check for new archive type
hpcarchive_pfx="rs_${runid_in}_${date_in}_"
hpcarchive=$(hpcarchive -p $archive_class $archive_user_arg -L -x -s -c "^$hpcarchive_pfx" | grep $hpcarchive_pfx | head -1)
if [[ $hpcarchive == "" ]] ; then
  # if not found, try "mc" archive
  echo "WARNING: no HPC archive is found with prefix: $hpcarchive_pfx!"
  hpcarchive_pfx="mc_${runid_in}_${date_in}_"
  echo "Try another prefix: $hpcarchive_pfx"
  hpcarchive=$(hpcarchive -p $archive_class $archive_user_arg -L -x -s -c "^$hpcarchive_pfx" | grep $hpcarchive_pfx | head -1)
  if [[ $hpcarchive == "" ]] ; then
    echo "ERROR: no HPC archive is found with this prefix: $hpcarchive_pfx!"
    exit 1
  fi
fi
# extract archive name and user from the retrieved listen
hpcarcuser=$(echo $hpcarchive | cut -f1 -d' ' | cut -c2-)
hpcarcname=${hpcarchive##* }

# pull full tape archive
echo hpcarchive -p $archive_class -u ${hpcarcuser} -r -c ${hpcarcname}
     hpcarchive -p $archive_class -u ${hpcarcuser} -r -c ${hpcarcname}
if is_directory ${hpcarcname}; then
  mv ${hpcarcname}/* .
fi

if (( save_agcm_files == 1 )); then

    # extract PARM/PARC from model job
    xtrct_parmc MODEL_JOB

    # Save PARMC
    cat PARM PARC > PARMC
    save PARMC mc_${runid_out}_${date_out}_par
    release PARMC

    # extract value of delt (this likely isn't needed)
    eval $(grep delt parc_out)
    delt=$(trim_whitespace $delt)
    echo delt=$delt

    # check for new bundled agcm restart
    agcm_restart_in=mc_${runid_in}_${date_in}_${agcm_restart_identifier}
    agcm_restart_out=mc_${runid_out}_${date_out}_${agcm_restart_identifier}
    if ! is_directory ${agcm_restart_in}; then
        # THIS IS FOR LEGACY RESTARTS AND SHOULD BE REMOVED ONCE ALL RESTARTS ARE IN NEW FORMAT

        # new bundled format doesn't exist - create nominal working version
        mkdir ${agcm_restart_out}
        cd ${agcm_restart_out}

        # pull in RS (AGCM) file
        cp ../mc_${runid_in}_${date_in}_rs_nmf ${agcm_restart_identifier}_OLDRS

        # pull in TS (CTEM) file
        cp ../mc_${runid_in}_${date_in}_ts_nmf ${agcm_restart_identifier}_OLDTS

        # If explicitly asked for, we try to access the namelists from the parent runid 
        #   - note: if not asked for, we only pull in the _required_ files 
        if (( ${#agcm_restart_namelists_to_use[@]} )); then                     # if this array has elements
            for nl_file in "${agcm_restart_namelists_to_use[@]}"; do
                # need to convert to lower name due to access/save behaviour that lowercases all letters
                nl_file_lower=$(echo $nl_file | tr '[:upper:]' '[:lower:]')
                nl_file_in_restart_bundle=${agcm_restart_identifier}_${nl_file}
                file_in_restart_archive="../mc_${runid_in}_${date_in}_${nl_file_lower}"

                # pull requested namelist file in so it can be extracted and used in the run (happens below)
                if is_file $file_in_restart_archive; then
                    echo "$nl_file available in retrieved archive!"
                    cp ${file_in_restart_archive} $nl_file_in_restart_bundle
                else
                    echo "$nl_file not available in retrieved archive! Will use the namelists that were already configured for this run"
                fi
            done
        fi
        cd $tmpdir
    else
        # we just need to rename the restart
        mv $agcm_restart_in $agcm_restart_out
    fi

    # prep files 
    #-----------
    cd $agcm_restart_out

    # pack files if necessary
    if [[ $agcm32bit == off ]]; then
        # pack TS and RS file
        echo "Packing RS and TS file!"
        for file_type in OLDTS OLDRS; do
            pakrs_float1 ${agcm_restart_identifier}_${file_type} tmp_${file_type}
            rm -f ${agcm_restart_identifier}_${file_type} 
            mv tmp_${file_type} ${agcm_restart_identifier}_${file_type} 
        done
    fi

    # store restart namelist files and use for this run, if requested
    if (( ${#agcm_restart_namelists_to_use[@]} )); then           # if this array has elements
        for nl_file in "${agcm_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=${agcm_restart_identifier}_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in!"
            else
                echo "Using $nl_file from agcm restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $agcm_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in but its not contained in \$agcm_namelists!"
                fi
            fi
        done
    fi
    cd $tmpdir

    # save restart
    #--------------
    save $agcm_restart_out $agcm_restart_out
    release $agcm_restart_out
fi

if (( save_coupler_files == 1 )); then
    coupler_restart_in="mc_${runid_in}_${date_in}_cplrs"
    coupler_restart_out="mc_${runid_out}_${date_out}_cplrs"

    if ! is_directory $coupler_restart_in; then
        # if bundle doesn't exist, create one
        mkdir $coupler_restart_out
        cd $coupler_restart_out
        cp ../cplrs_* . || bail "No coupler restart files available in the retrieved archive!"
    else
        # just rename 
        mv $coupler_restart_in $coupler_restart_out
    fi

    # if desired, pull namelists from restart and use for this run
    if (( ${#coupler_restart_namelists_to_use[@]} )); then
        cd $coupler_restart_out
        for nl_file in "${coupler_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=cplrs_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in!"
            else
                echo "Using $nl_file from coupler restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $coupler_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in but its not contained in \$coupler_namelists!"
                fi
            fi
        done
        cd $tmpdir
    fi

    # save
    save ${coupler_restart_out} ${coupler_restart_out}
    release ${coupler_restart_out}
fi

if (( save_nemo_files == 1 )) ; then
    # looks for nemo tar bundle
    nemo_restart_in_tarfile="mc_${runid_in}_${date_in}_nemors.tar"
    nemo_restart_out="mc_${runid_out}_${date_out}_nemors"
    if ! is_zero_size $nemo_restart_in_tarfile; then
        mkdir $nemo_restart_out
        cd $nemo_restart_out
        tar -xvf ../${nemo_restart_in_tarfile} >> /dev/null 2>&1
        cd $tmpdir
    else
        bail "No nemo restart available in the retrieved archive!"
    fi

    # if desired, pull namelists from restart and use for this run
    if (( ${#nemo_restart_namelists_to_use[@]} )); then
        cd $nemo_restart_out
        for nl_file in "${nemo_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=rs_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in!"
            else
                echo "Using $nl_file from nemo restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $nemo_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in but its not contained in \$nemo_namelists!"
                fi
            fi
        done
        cd $tmpdir
    fi

    # save
    save ${nemo_restart_out} ${nemo_restart_out}
    release $nemo_restart_out
fi

# save environment and jobstring
env > MODEL_ENV
save  MODEL_ENV mc_${runid_out}_${date_out}_env
save  MODEL_JOB mc_${runid_out}_${date_out}_jobstring

cd $call_dir

&main_coupler_par
  ! The following values are set by the make_job script and are static over the course of a run, and
  ! are only required by cpl_main
  env_runid = "default"                   ! The model's runid
  windstress_remap = "bilinear"           ! The regridding option to use for windstress
  landfrac_bug = .false.                  ! If true, retain the CanESM5, p1 land fraction errors in Antarctica
  cpl_rs_abort_if_missing_field = .false. ! If true, error out if the restart file is missing a field
  bulk_in_cpl = .false.                   ! If true, calculate fluxes within the coupler using the bulk formulae
  zero_runoff_sent_to_ocean = .false.     ! If true, runoff will be set to zero within the coupler
  ! These are set in cpl_prelude because they might change every resubmission
  env_model = "default"                   ! An identifying string for the model name
  env_start = "default"                   ! An identifying string for the model start date
  env_run_start_year = 5550               ! The first year of the model run
  env_run_start_month = 01                ! The first month of the model run
  env_run_stop_year = 5550                ! The last year of the model run
  env_run_stop_month = 12                 ! Last month of the model run
  cpl_use_initial_conditions = .false.    ! If true, the coupler is initialized from initial conditions
/

&comm_coupler_par
  ! the follow values are configuration parameters that are required across multiple programs
  specified_bc_year_offset = 1            ! Value added to the year in the specified boundary conditions (AMIP-only)
  couple_serial = .false.                 ! If true, the AGCM and ocean are stepped sequentially
/

! Regridding options for coupler to ocean fluxes. Valid options include:
!   conservative, conserve_2nd, bilinear, patch, nearest_stod, nearest_dtos
!   Reference:http://www.earthsystemmodeling.org/esmf_releases/last_built/esmpy_doc/html/RegridMethod.html
&ocean_regrid
  momentum_flux = "bilinear"     ! Wind stresses
  water_flux    = "conservative" ! Freshwater fluxes
  heat_flux     = "conservative" ! Heat fluxes
  windspeed     = "conservative" ! Wind speed modulus
  ice_melt      = "conservative" ! Top and bottom ice melt
  pressure      = "conservative" ! Surface pressure
  tracer        = "conservative" ! Atmospheric tracer concentrations
/

&comm_coupler_horz_dims
  ! The following variables define the common horizontal grid sizes within the coupler that are used.
  ! These are required across multiple programs.
  nlon_a = 128              ! number of points in the zonal direction for the atmosphere
  nlat_a = 64               ! number of poitns in the meridional direction for the atmosphere
  olap_a = 1                ! number of points that overlap in the zonal direction for the atmosphere
  nlon_o = 360              ! number of points in the zonal direction for the ocean
  nlat_o = 292              ! number of points in meridional direction for the ocean
  olap_o = 2                ! number of points that overlap in the zonal direction for the ocean
  nlon_i = 360              ! number of points in the zonal direction for the ice
  nlat_i = 292              ! number of points in meridional direction for the ice
  olap_i = 0                ! number of points that overlap in the zonal direction for the ice
  nlon_canom = 256          ! number of points in the zonal direction for the OLD ocean (slated for removal)
  nlat_canom = 192          ! number of points in meridional direction for the OLD ocean (slated for removal)
  olap_canom = 1            ! number of points that overlap in the zonal direction for the OLD ocean (slated for removal)
/

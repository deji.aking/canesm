#!/bin/bash
# Set environment variables used by make_job to select the CanESM "model variant" to use. A model variant is distinct
# from a runmode in that it fundamentally affects the setup of the model across a variety of CMIP-like experiments.
# These typically correspond to major and minor versions of CanESM along with the associated 'p' number.

# Throw an error for an invalid model variant
function error_model_variant {

  error_type=$1
  case ${error_type,,} in
    "variant")
      echo "Invalid model_variant = $model_variant"
      exit 1
      ;;
    *)
      echo "Generic error in model_variants.sh"
      exit 99
      ;;
  esac
}

# Set the shell variables that set the compile and runtime options that determine the model variant
function set_model_variant {

  model_variant="$1-$2"

  case $model_variant in
    5.0*)
        water_flux='conservative'
        heat_flux='conservative'
        windspeed='conservative'
        ice_melt='conservative'
        pressure='conservative'
        tracer='conservative'
      case $model_variant in
        5.0-p1)
          landfrac_bug=.true.
          momentum_flux='conservative'
          ;;
        5.0-p2)
          landfrac_bug=.false.
          momentum_flux='bilinear'
          ;;
        *)
          error_model_variant 'variant'
          ;;
      esac
      ;;
    5.1-p1)
        landfrac_bug=.false.
        momentum_flux='bilinear'
        water_flux='conservative'
        heat_flux='conserve_2nd'
        windspeed='bilinear'
        ice_melt='conserve_2nd'
        pressure='bilinear'
        tracer='bilinear'
        ESMF_V8=1
        ;;
    *)
        error_model_variant 'variant'
        ;;
  esac
}

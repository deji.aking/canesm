#! /bin/bash
set -e
# Compilation script for CanESM5

#==============
# Define usage
#==============
Usage='Usage:

  compile-canesm.sh [-d] [-a] [-c] [-n] [-f] [-h] [config_file=/path/to/config/file] [opt_level=debug/repro/opt]

  Flags:
      -d compile CanDIAG
      -a compile CanAM
      -c compile CanCPL
      -n compile CanNEMO
      -f force a clean compilation
      -l copy the code compile code locally, as opposed to in the source repository
      -h display this usage message!

  If none of the flags are specified, compile all components for the config specified in the 
  main config file, reusing files that may have already been generated.

  Arguments:
    config_file :   (optional) file containing the configuration parameters to be used in this compilation.
                               Defaults to canesm.cfg in the current working directory.
    opt_level   :   (optional) specify additional optimization level information for component (agcm/nemo/coupler) compilation
                                "debug" -> use debugging symbols during compilation (for use with DDT or other debuggers)
                                "repro" -> reduce optimization to allow for reproducible settings 
                                "opt"   -> use optimization compilation settings 
                               If not specified, compilation defaults to using settings equivalent to "opt".
'

#=====================
# Set Default Options
#=====================
compile_diags=0
compile_atmos=0
compile_cpl=0
compile_nemo=0
local_compilation=0
user_defined_compilation=0
clean_compile=0
optimization_level="default" # can be overwritten at CLI or in the config_file

#==============
# Parse CL Args
#==============
# FLAGS
while getopts dacnflh opt; do
    case $opt in
        d) compile_diags=1; user_defined_compilation=1 ;;
        a) compile_atmos=1; user_defined_compilation=1 ;;
        c) compile_cpl=1;   user_defined_compilation=1 ;;
        n) compile_nemo=1;  user_defined_compilation=1 ;;
        f) clean_compile=1      ;;
        l) local_compilation=1  ;;
        h) echo "$Usage"; exit    ;;
        ?) echo "$Usage"; exit 1  ;;
    esac
done
shift $(( OPTIND - 1 ))

# ARGUMENTS
for arg in "$@"; do
    case $arg in
        *=*)
            var=$(echo $arg | awk -F\= '{printf "%s",$1}')
            val=$(echo $arg | awk -F\= '{printf "%s",$2}')
            case $var in
                config_file) user_defined_canesm_cfg_file="$val"        ;; # user/CLI defined config file
                    opt_lvl) user_defined_optimization_level="$val"     ;; # user/CLI defined optimization level
                          *) echo "Invalid command line arg --> $arg <--"
                             echo "$Usage"
                             exit 1 ;;
            esac ;;
          *) echo "Invalid command line arg --> $arg <--"
             echo "$Usage"
             exit 1 ;;
    esac
done

#===================================
# Source high level config settings 
#===================================
if [[ -z "$user_defined_canesm_cfg_file" ]]; then
    canesm_cfg_file=canesm.cfg
else
    canesm_cfg_file=$user_defined_canesm_cfg_file
fi
. ${canesm_cfg_file}

# override the optimization level with CLI option, if necessary
if ! [[ -z "$user_defined_optimization_level" ]]; then
    optimization_level=$user_defined_optimization_level
fi

# Make sure the compilation script has the correct variables
[[ -z "$WRK_DIR" ]]             && { echo "WRK_DIR must be defined and point to the setup directory! Check your config/envionment files"; exit 1; }
[[ -z "$EXEC_STORAGE_DIR" ]]    && { echo "EXEC_STORAGE_DIR must be defined! Check your config/environment files"; exit 1; }
[[ -z "$CANESM_SRC_ROOT" ]]     && { echo "CANESM_SRC_ROOT must be defined! Check your config/environment files"; exit 1; }
[[ -z "$COMPILER" ]]            && { echo "COMPILER must be defined! Check your config/environment files"; exit 1; }
[[ -z "$PLATFORM" ]]            && { echo "PLATFORM must be defined! Check your config/environment files"; exit 1; }
[[ -z "$RUNID" ]]               && { echo "RUNID must be defined! Check your config/environment files"; exit 1; }
[[ -z "$CONFIG" ]]              && { echo "CONFIG must be defined! Check you confi/environment files"; exit 1; }

#=========================
# Set inherited variables
#=========================
# define target names
agcm_target=${EXEC_STORAGE_DIR}/${agcm_exec}
cpl_target=${EXEC_STORAGE_DIR}/${coupler_exec}
nemo_target=${EXEC_STORAGE_DIR}/${nemo_exec}
diag_bin_dir=${EXEC_STORAGE_DIR}/bin_diag

# define cpp key files
cppdef_config_file=${WRK_DIR}/config/cppdef_config.h
cppdef_sizes_file=${WRK_DIR}/config/cppdef_sizes.h

# determine the targets for this config, and if necessary, determine what to compile
case $CONFIG in
    ESM) targets="$agcm_target $cpl_target $nemo_target";
         [[ $user_defined_compilation -eq 0 ]] && { compile_atmos=1; compile_cpl=1; compile_nemo=1; compile_diags=1; } ;;
   AMIP) targets="$agcm_target $cpl_target";
         [[ $user_defined_compilation -eq 0 ]] && { compile_atmos=1; compile_cpl=1; compile_diags=1; } ;;
   OMIP) targets="$nemo_target";
         [[ $user_defined_compilation -eq 0 ]] && { compile_nemo=1; } ;;
      *) echo "Unknown 'CONFIG' value! config must equal one of ESM/AMIP/OMIP! Check your config/environment files"; exit 1;;
esac
echo "compile_diags=$compile_diags"
echo "compile_atmos=$compile_atmos"
echo "compile_cpl=$compile_cpl"
echo "compile_nemo=$compile_nemo"

# Detect the optimization level for mkmf-based builds
case $optimization_level in
    'debug'|'none')
        MAKE_CMD="make DEBUG=1"
        nemo_opt_suffix="-debug"
        ;;
    'repro')
        MAKE_CMD="make REPRO=1"
        nemo_opt_suffix="-repro"
        ;;
    'opt'|'default')
        MAKE_CMD="make OPT=1"
        nemo_opt_suffix=""
        ;;
    *)
        echo "Unsupported optimization level! Must be one of none/debug/repro/opt/default."; exit 1
        ;;
esac


# determine where the compilation will happen
if (( local_compilation == 1 )); then
    # compile locally
    canesm_src="$(pwd)/tmp_canesm_build"
    echo "compiling in a local, temporary, source location: $canesm_src"
    cp -rp ${CANESM_SRC_ROOT} ${canesm_src}
else 
    # compile in the source location
    canesm_src=${CANESM_SRC_ROOT}
fi

#==================================
# Setup the compilation environment
#==================================
compilation_env_file=${WRK_DIR}/config/compilation_environment
source $compilation_env_file

#==========================================================
# Run code checking and update logs for ECCC platforms
#==========================================================
if (( CODE_CHECKS == 1 )) ; then 
    [[ $production -eq 1 ]] && flgs="" || flgs="-d"   # use 'development' mode if production is off
    strict_check $flgs $RUNID compile canesm.cfg compile_${RUNID}
    [[ $? -ne 0 ]] && exit 1
fi

#===========================
# Compile Desired Components
#===========================
mkdir -p ${EXEC_STORAGE_DIR}
#-------------------------------
# Compile Diagnostics / Programs
#-------------------------------
if (( compile_diags == 1 )) ; then

    # compare against reference library at CANDIAG_REF_PATH if it exists
    if [[ -n $CANDIAG_REF_PATH ]] && [[ -d $CANDIAG_REF_PATH ]]; then
        candiag_ref_exists=1
    else
        candiag_ref_exists=0
    fi

    if (( candiag_ref_exists == 1 )); then
        user_candiag_src=${canesm_src}/CanDIAG
        user_tmp_md5sum_file=${WRK_DIR}/.user_candiag_md5sums
        ref_tmp_md5sum_file=${WRK_DIR}/.ref_candiag_md5sums
        candiag_updates=0

        # create a file that contains the checksum info for every file in user/reference diag source dir
        get_candiag_checksum_file(){
            src_dir=$1
            md5sum_file=$2
            invoke_dir=$(pwd)
            cd $src_dir
            find lssub lspgm -type f -exec md5sum {} + | sort -k 2 > $md5sum_file
            md5sum diag_sizes.f90 >> $md5sum_file
            cd $invoke_dir
        }
        get_candiag_checksum_file $user_candiag_src $user_tmp_md5sum_file
        get_candiag_checksum_file $CANDIAG_REF_PATH $ref_tmp_md5sum_file

        # check for differences
        diff -u $user_tmp_md5sum_file $ref_tmp_md5sum_file > /dev/null 2>&1 || candiag_updates=1
        rm -f $user_tmp_md5sum_file
        rm -f $ref_tmp_md5sum_file
    fi

    # if there are differences, recompile, else use precompiled version
    CanDIAG_build_dir=${canesm_src}/CanDIAG/build
    cd $CanDIAG_build_dir
    if (( candiag_updates == 1 || clean_compile == 1 || candiag_ref_exists == 0 )) ; then
        echo "Compiling CanDIAG..."

        # check if the reference library was previously linked in
        if [[ -L "bin" ]]; then
            make clean 2> /dev/null # swallow warnings about no arguments
        fi

        # clean if user requested it
        if (( clean_compile == 1 )); then 
            make clean 2> /dev/null # swallow warnings about no arguments
        fi

        # link in necessary make template
        make_template=make.template.${COMPILER}
        [[ -e $make_template ]] || ln -s ${WRK_DIR}/config/${make_template}

        # compile
        make COMPILER_TEMPLATE=${make_template} \
            -j 4 > ${WRK_DIR}/.compile-canesm-candiag-$$.log 2>&1

        echo "Compiling CanDIAG - DONE!"
    else
        echo "Using CanDIAG Reference Library..."
        make clean 2> /dev/null # clean existing links/dirs and swallow warnings about no arguments
        ln -s ${CANDIAG_REF_PATH}/build/bin bin
        ln -s ${CANDIAG_REF_PATH}/build/lib64 lib64
        ln -s ${CANDIAG_REF_PATH}/build/lib32 lib32
    fi
    cd $WRK_DIR
fi

#---------------
# Compile CanAM
#---------------
if (( compile_atmos == 1 )) ; then
    echo "Compiling CanAM..."
    AGCM_build_dir=${canesm_src}/CanAM/build

    if (( clean_compile == 1 )); then
        ( cd $AGCM_build_dir; make clean 2> /dev/null) # hide warnings from make caused by zero arguments
    fi

    # compile and place into background
    (   cd $AGCM_build_dir;                                                     \
        $MAKE_CMD agcm32bit=$agcm32bit                                          \
             CPP_CONFIG_FILE=$cppdef_config_file                                \
             CPP_SIZES_FILE=$cppdef_sizes_file                                  \
             MKMF_TEMPLATE=${WRK_DIR}/config/mkmf.template.${COMPILER}          \
             > ${WRK_DIR}/.compile-canesm-canam-$$.log 2>&1                     \
    ) &
fi

#----------------
# Compile CanCPL
#----------------
if (( compile_cpl == 1 )) ; then
    echo "Compiling CanCPL..."
    CPL_build_dir=${canesm_src}/CanCPL/build

    if (( clean_compile == 1 )); then
        ( cd $CPL_build_dir; make clean 2> /dev/null ) # hide warnings from make caused by zero arguments
    fi

    # compile and place into background
    (   cd $CPL_build_dir;                                                      \
        $MAKE_CMD CPP_CONFIG_FILE=$cppdef_config_file                           \
             CPP_SIZES_FILE=$cppdef_sizes_file                                  \
             ESMF_V8=$ESMF_V8                                                   \
             MKMF_TEMPLATE=${WRK_DIR}/config/mkmf.template.${COMPILER}          \
             > ${WRK_DIR}/.compile-canesm-cancpl-$$.log 2>&1                    \
    ) &
fi

#-----------------
# Compile CanNEMO
#-----------------
if (( compile_nemo == 1 )) ; then
    echo "Compiling CanNEMO..."
    # build array of arguments for build-nemo
    #   -> array needed to handle space delimited list that may come from add_key/del_key
    NEMO_repo_path=${canesm_src}/CanNEMO
    nemo_opts=(     srcpath=${NEMO_repo_path} )
    nemo_opts+=(    exec=${nemo_exec} )
    nemo_opts+=(    cfg=${nemo_config} )
    nemo_opts+=(    nemo_arch=${COMPILER}.${PLATFORM}${nemo_opt_suffix} )
    nemo_opts+=(    util_arch=${WRK_DIR}/config/make.template.${COMPILER} )
    nemo_opts+=(    comp_env=${compilation_env_file} )
    [[ -n "$nemo_add_cpp_keys" ]] && nemo_opts+=( add_key="${nemo_add_cpp_keys}" )
    [[ -n "$nemo_del_cpp_keys" ]] && nemo_opts+=( del_key="${nemo_del_cpp_keys}" )
    (( clean_compile == 1 ))      && nemo_opts=( "-f" "${nemo_opts[@]}")
    (( local_compilation == 1 ))  && nemo_opts=( "-c" "${nemo_opts[@]}")

    # compile nemo executable
    echo 'build-nemo -p $EXEC_STORAGE_DIR -j 12 '"${nemo_opts[@]}" > ${WRK_DIR}/.compile-canesm-cannemo-$$.log
    build-nemo -p $EXEC_STORAGE_DIR -j 12 "${nemo_opts[@]}" >> ${WRK_DIR}/.compile-canesm-cannemo-$$.log 2>&1 &
fi

wait
echo "Compiling ALL - DONE!"
echo "Check for compilation errors in .compile-canesm-* !"

#================================================
# Store Executables (ocean done within build-nemo)
#================================================
if (( local_compilation == 1 )); then
    # hard copy executables 
    (( compile_atmos == 1 )) && cp -f ${AGCM_build_dir}/bin/${agcm_exec} $agcm_target
    (( compile_cpl == 1 ))   && cp -f ${CPL_build_dir}/${coupler_exec} $cpl_target
    (( compile_diags == 1 )) && cp -rf ${CanDIAG_build_dir}/bin $diag_bin_dir

    # clean up temporary local repo
    rm -rf ${canesm_src} 
else
    # use links
    (( compile_atmos == 1 )) && { rm -f $agcm_target  ; ln -s ${AGCM_build_dir}/bin/${agcm_exec} $agcm_target ; }
    (( compile_cpl == 1 ))   && { rm -f $cpl_target   ; ln -s ${CPL_build_dir}/${coupler_exec} $cpl_target ; }
    (( compile_diags == 1 )) && { rm -f $diag_bin_dir ; ln -s ${CanDIAG_build_dir}/bin $diag_bin_dir ; }
fi

#============================================
# Make sure all desired targets were created
#============================================
function is_file_or_valid_link(){
    local obj
    local exit_status
    obj=$1
    if [[ -L $obj ]]; then
        obj=$(readlink $obj)
    fi
    if [[ -e $obj ]]; then
        exit_status=0
    else
        exit_status=1
    fi
    return $exit_status
    }
if (( compile_atmos == 1 )); then
    is_file_or_valid_link ${agcm_target} || echo "Failed to build agcm target!"
fi
if (( compile_nemo == 1 )); then
    is_file_or_valid_link ${nemo_target} || echo "Failed to build nemo target!"
fi
if (( compile_cpl == 1 )); then
    is_file_or_valid_link ${cpl_target} || echo "Failed to build coupler target!"
fi
if (( compile_diags == 1 )); then
    is_file_or_valid_link ${diag_bin_dir} || echo "Failed to build candiag binaries!"
fi

